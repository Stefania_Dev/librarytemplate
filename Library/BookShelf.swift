//
//  BookShelf.swift
//  Library
//
//  Created by Stefania Zinno on 09/12/2019.
//  Copyright © 2019 Stefania Zinno. All rights reserved.
//

import UIKit
import CloudKit

class BookShelf: UITableViewController {
    var refresh: UIRefreshControl!
    var retrievedRecords : [CKRecord] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CKManager.shared.subscribeToAddBook()        
        CKManager.shared.fetchReader { (result) in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let record):
                CKManager.shared.reader = record.first!
            }
                
            }
        refreshData()
        refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refresh)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: NSNotification.Name(rawValue: "bookDidAdded"), object: nil)
    }
    
    
    @objc func refreshData(){
        CKManager.shared.fetchRecords { (result) in
            switch result {
            case .success(let records):
                self.retrievedRecords = records
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.refresh.endRefreshing()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return retrievedRecords.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookSegue", for: indexPath) as! BookShelfCell
        cell.title.text = retrievedRecords[indexPath.row]["Title"] as? String
        cell.author.text = retrievedRecords[indexPath.row]["Author"] as? String
        cell.genre.text = retrievedRecords[indexPath.row]["Genre"] as? String
        let asset = retrievedRecords[indexPath.row]["Cover"]  as! CKAsset?
        if let asset = asset {
            let photoData : NSData? = NSData(contentsOf:asset.fileURL!)
            cell.cover.image = UIImage(data:photoData! as Data)
            
        }
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let rowRecord = self.retrievedRecords[indexPath.row]
            let title = "You are deleting a record."
            let message = "Are you sure you want to delete this item?"
            let deleteActionController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { action in
                
                CKManager.shared.deleteRecord(recrdoToDelete: rowRecord) { (result) in
                    switch result {
                    case .failure(let error):
                        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    case .success(let records):
                        DispatchQueue.main.async {
                            self.retrievedRecords.remove(at: indexPath.row)
                            self.tableView.deleteRows(at: [indexPath], with: .automatic)
                        }
                    }
                }
            })
            deleteActionController.addAction(cancelAction)
            deleteActionController.addAction(deleteAction)
            self.present(deleteActionController, animated: true, completion: nil)
        } else if editingStyle == .insert {
        }
        
    }
    
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "modify" {
            
        }
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     
    
    
    
    
}
