//
//  CKManager.swift
//  Library
//
//  Created by Stefania Zinno on 09/12/2019.
//  Copyright © 2019 Stefania Zinno. All rights reserved.
//

import UIKit
import CloudKit

class CKManager {    
    static let shared = CKManager()
    let publicDatabase = CKContainer.default().publicCloudDatabase
    var reader = CKRecord(recordType: "Reader")
    
    //    MARK: fetchRecords
    func fetchRecords (completion: @escaping ( Result<[CKRecord], Error>) -> Void){
        
        
    }
    //    MARK: deleteRecord
    func deleteRecord (recrdoToDelete: CKRecord, completion: @escaping ( Result<CKRecord.ID, Error>) -> Void){
    
        
    }
    //    MARK: saveRecord
    func saveRecord (outletCollection: [UITextField], coverToSave: UIImage?, completion: @escaping (Result<CKRecord, Error>) -> Void){
        

    }
    
    //    MARK: modifiyRecord
    func modifiyRecord (rowRecord: CKRecord, completion: @escaping (Result<CKRecord, Error>) -> Void){
     
    }
    
    //    MARK: fetchReader
    func fetchReader (completion: @escaping ( Result<[CKRecord], Error>) -> Void){
  
    }
    
    //MARK: - Subscriptions
    func subscribeToAddBook (){
        
    }
    
}



